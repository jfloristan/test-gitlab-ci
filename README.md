# Test GitLab CI pipelines in Openshift

This repository contains the steps to deploy a simple application in Openshift using
Gitlab CI/CD Pipelines and specific GitLab runners

_NOTE: It has been tested in OCP 3.11, but it may work in several previous versions as well

Requirements:

 * Openshift cluster up and running
 
 * Gitlab Runners available in your Gitlab project and with connection to your Openshift cluster
    * Check here how to run Gitlab runners as elastic containers in Openshift: https://gitlab.com/jfloristan/ocp-gitlab-runners.git
    * Or check how to install Gitlab runners in a linux box in the script: gitlab-runner-install/install-gitlab-runner-linux.sh
    
 * A project configured in Openshift called: test-gitlab-ci
   ```console 
   $ oc new-project test-gitlab-ci --description="Test Gitlab CI in OCP" --display-name="Test Gitlab CI in OCP"
   ```
  * A service account that can edit objects in your project
   ```console 
   # enter the test-gitlab-ci project
   $ oc project test-gitlab-ci
   # create the service account
   $ oc create sa gitlab-ci
   # add edit role to service account
   $ oc policy add-role-to-user edit system:serviceaccount:test-gitlab-ci:gitlab-ci
   ```
  * The token of service account to authebticate 
   ```console 
   $ oc get secret | grep gitlab-ci-token.*.service-account-token
   $ oc describe secret gitlab-ci-token-4rt6b
   ```  

# Configure the in Gitlab

Go to your Gitlab project dashboard and in Settings -> CI/CD -> Variables and configure the following
variables:

```console
 OPENSHIFT_TOKEN    Value: Token for your Openshift service account in your Openshift project 
 OPENSHIFT_SERVER   Value: Openshift server address i.e. https://192.168.99.103:8443
 OPENSHIFT_DOMAIN   Value: Openshift domain  i.e  192.168.99.103.nip.io
```



